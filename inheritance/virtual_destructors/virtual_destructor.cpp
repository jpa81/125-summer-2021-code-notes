// virtual_destructor.cpp

#include "cmpt_error.h"
#include <iostream>

//
// This code is based in printable2.cpp, and is meant to help demonstrate the
// need for virtual destructors.
//

using namespace std;

class Printable {
public:
    // It's necessary to define the print() method here since it is used in
    // the println method that follows. It is "= 0" to indicate that it has no
    // default implementation, and it is "virtual" to indicate that a class
    // that inherits from Printable can provide it's own instance of print().
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

    // Non-virtual destructor for test purposes ... this causes an error!
    ~Printable() {
        cout << "Printable destructor called\n";
    }
}; // class Printable

class Point : public Printable {
private:
    double x;
    double y;

public:
    // default constructor
    Point() : x(0), y(0) { }

    // copy constructor
    Point(const Point& other) : x(other.x), y(other.y) { }

    Point(double a, double b) : x(a), y(b) { }

    // getters
    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const {
        cout << '(' << x << ", " << y << ')';
    }

    ~Point() {
        cout << "Point: destructor called\n";
    }
}; // class Point

int main() {
    Point a(1, 2);
    a.println();
    // (1, 2)
    // Point: destructor called
    // Printable destructor called

    // Point* b = new Point(3, 4);
    // b->println();
    // delete b;
    // // (3, 4)
    // // Point: destructor called
    // // Printable destructor called

    // Printable* c = new Point(3, 4);  // notice the difference from b!
    // c->println();
    // delete c;
    // // (3, 4)
    // // Printable destructor called
}
