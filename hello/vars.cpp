// vars.cpp

#include <iostream>

using namespace std;

int main() 
{
	// variable definitions
	int a; // a is assigned an unknown int
    cout << a << "\n";

	// unsigned int b = 23;    // 23 is an int literal
	// char         c = 'm';   // 'm' is a char literal
	// float        d = 1.2;   // 1.2 is a double literal
	// double       e = 1.996; // 1.996 is a double literal

	// auto f = 'a';  // f is defined to be of type char

	// cout << a << "\n";
	// cout << b << "\n";
	// cout << c << "\n";
	// cout << d << "\n";
	// cout << e << "\n";
	// cout << f << "\n";

	// cout << "\n";
	// cout << char(c + 1) << "  "
	//      << int(c+1)   // c+1 is the char 'n'
	//      << "\n";
	// cout << char(109)
	//      << "\n";

	// cout << "\n";
	// cout << float(a)              // C++-style case
	//      << "\n";
 //    cout << static_cast<float>(a) // C++-style case
 //         << "\n";
	// cout << (float)a              // C-style case
	//      << "\n";

	// cout << "\n";
	// // d == 1.996
	// cout << int(d)              // C++-style case
	//      << "\n";
 //    cout << static_cast<int>(d) // C++-style case
 //         << "\n";
	// cout << (int)d              // C-style case
	//      << "\n";

	// cout << "\n";
	// cout << static_cast<unsigned int>(a) // int a = -4;
	//      << "\n";
	// cout << static_cast<unsigned int>(-d) // 1.2
	//      << "\n";
	// cout << static_cast<unsigned int>(-e) // 1.996
	//      << "\n";

} // main
