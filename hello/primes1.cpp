// primes1.cpp

//
// Tests if an integer entered by the user is prime number.
//
// An integer n is a prime number if it is greater than 1, and has exactly two
// different divisors, 1 and n. The first few primes are 2, 3, 5, 7, 11, ...,
// Note that 2 is the only even prime number.
//

#include <iostream>

using namespace std;

bool is_prime(int n) {
    if (n < 2) {
        return false;
    } else if (n == 2) {
        return true;
    } else if (n % 2 == 0) {
        return false;
    } else {
        // n > 2, and n is odd
        int trial_divisor = 3;
        while (trial_divisor * trial_divisor <= n) {
            if (n % trial_divisor == 0) {
                return false;
            }
            trial_divisor += 2;
        }
        return true;
    }
}

// A composite prime is an integer greater than 1 that is not prime, i.e. an
// integer n greater than 1 is composite if it has at least one divisor other
// than 1 and n.
bool is_composite(int n) {
    return n > 0 && !is_prime(n);
}


int main() {
    const int MAX = 101;
    int count = 0;
    int i = 0;
    while (i < MAX) {
        if (is_composite(i)) {
            cout << i << "\n";
            count++;
        }
        i++;
    }
    cout << count << " composites less than " << MAX << "\n";
} // main
