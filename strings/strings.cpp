// strings.cpp

#include <iostream>
#include <string>

using namespace std;

void example1() {
	string s = "Jane";
	string t = "Goodall";
	if (s < t) {
		cout << s << " comes before " << t << " alphabetically\n";
	} 
    string name = s + " " + t;
    cout << name << "\n";
}

void example2() {
	string s = "hamster";
	for(int i = 0; i < s.size(); i++) {
		cout << "s[" << i << "] = '" << s[i] << "'\n";
	}

	// for-each loop
	for(char c : s) {
		cout << c << " ";
	}
	cout << "\n";
}
	
int main() {
	example1();
	example2();
}
