[sfml_test_mouse.cpp](sfml_test_mouse.cpp) is a sample SFML program that lets
the user draw on the screen using the mouse. To draw, hold down the left mouse
button and move the mouse.

To install SFMl on Ubuntu Linux, type this into a terminal window:

```bash
$ sudo apt-get libsfml-dev
```

To compile and run the program, first make sure both
[sfml_test_mouse.cpp](sfml_test_mouse.cpp) and [makefile](makefile) are in the
same folder on your Linux machine. Then go to that folder in your terminal and
type `make`:

```bash
$ make
g++ -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g -c sfml_test_mouse.cpp
g++ sfml_test.o -o sfml_test -lsfml-graphics -lsfml-window -lsfml-system
```

To run the program, type `./sfml_test_mouse`:

```bash
$ ./sfml_test_mouse
... draw with the mouse ...
```

The [SFML documentation](https://www.sfml-dev.org/documentation/2.5.1/) is all
online. The [SFML list of
classes](https://www.sfml-dev.org/documentation/2.5.1/annotated.php) is a good
place to look to find documentation about various features. For example, [the
`CircleShape`](https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1CircleShape.php)
documenetation lists all the features of a `CircleShape`.
