// swap.cpp

#include <iostream>

using namespace std;

// x and y are passed by value, i.e. a copy of the passed-in values are passed
// to swap_bad
void swap_bad(int x, int y) {
	int temp = x;
	x = y;
	y = temp;
}

// x and y are passed by refernce, i.e. a reference to the actual passed-in
// values are passed, and no copy is made
void swap_good(int& x, int& y) {
	int temp = x;
	x = y;
	y = temp;
}

int main() {
	int a = 2;
	int b = 5;
	cout << "a: " << a << ", b: " << b << "\n";
	cout << "swapping a and b ...\n";
	swap_good(a, b);
	cout << "a: " << a << ", b: " << b << "\n";
}
