// primes5.cpp

//
// Based on primes4.cpp: added num_primes_less_than function.
//
// An integer n is a prime number if it is greater than 1, and has exactly two
// different divisors, 1 and n. The first few primes are 2, 3, 5, 7, 11, ...,
// Note that 2 is the only even prime number.
//

#include <iostream>

using namespace std;

// Returns true if n is a prime number, and false otherwise.
bool is_prime(int n) {
	if (n < 2) {             // numbers less than 2 are not prime
		return false;
	} else if (n == 2) {     // 2 is the smallest prime number
		return true;
	} else if (n % 2 == 0) { // all other even numbers are not prime
		return false;
	} else {
		// At this point we know n is an odd positive number. To test if it's
		// prime, we check to see if any of the odd numbers 3, 5, 7, ... up to
		// the square root of n are divisors of n. If none of them do, then n
		// must be prime.
		//
		// Note that this is *not* the most effcient way to test if a number
		// is prime, but it is good enough for our purposes.
		int trial_divisor = 3;
		while (trial_divisor * trial_divisor <= n) {
			if (n % trial_divisor == 0) {
				return false;
			}
			trial_divisor += 2;
		}
		return true;
	}
}

int num_primes_less_than(int n) {
	int count = 0;
	for(int i = 1; i < n; i++) {
		if (is_prime(i)) {
			count++;
		}
	}
	return count;
}

int main() {
    const int MAX = 1000000000;
	cout << num_primes_less_than(MAX) 
	     << " primes from 1 to " << MAX << "\n";
} // main

//
// With no optimizations, time to count the number of primes less than one
// billion:
//
// $ time ./primes5
//
// 50847534 primes from 1 to 1000000000
//
// ________________________________________________________
// Executed in   18.98 mins   fish           external 
//    usr time  1137.88 secs  156.00 micros  1137.88 secs 
//    sys time    0.20 secs  155.00 micros    0.20 secs 
//
// Turning on -O2 or -O3 optimizations don't make any major difference in
// run-time.
//
