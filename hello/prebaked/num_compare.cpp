// num_compare.cpp

#include <iostream>

using namespace std;

int main() {
	cout << "Enter two numbers: ";
	double a, b;
	cin >> a >> b;

	if (a == b) {
		cout << a << " and " << b << " are the same\n";
	} else if (a < b) {
		cout << a << " is less than " << b << "\n";
	} else {
		cout << a << " is greater than " << b << "\n";
	}
}
